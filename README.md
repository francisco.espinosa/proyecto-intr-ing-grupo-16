Repetidor Wifi + Servidor de archivos compartidos en Raspberry Pi 4B
------------
### Introducción

Sin duda la conexión Wifi, es uno de los mejores inventos de la humanidad, ya que nos permite la conexión a internet de manera inalambrica. Se podría decir que teniendo una conexión Wifi , en nuestro hogar u oficina, seriamos capacez de acceder a la red desde cualquier lugar que se encuentre dentro del rango de alcance de esta. Pero ¿que pasa con aquellas  zonas muertas, aquellas en las que la señal de nuestro router no llega?
Bueno, de ahí surge la idea de la implementación de un repetidor Wifi. Éste, además de ampliar nuestra conexión a la red Wifi que proviene de nuestro Router, también nos ayuda a mejorar la calidad y la estabilidad de la señal en otras zonas donde la conexión sea muy débil.  Si deseas ampliar tu conexión de red wifi con un repetidor Wifi, sigue leyendo esto, ya que frabricaremos uno con una Raspberry Pi 4. Ademas, agregaremos un servidor de archivos compartidos, que nos servira para almacenar archivos, transferirlos o acceder a ellos , con ciertos permisos de usuario, desde nuestra Raspberry a otro ordenador que se encuentre conectado a nuestra red.


------------


#### Hardware  requerido
- Raspberry Pi 4 de 2GB RAM
- Tajeta Micro SD 16 GB
- Otro dispositivo de almacenamiento adicional (Disco duro un 1TB o mas, (recomendado))

------------


#### Software requerido
- Sistema operativo Rasbian

------------



### Tutorial
Lo primero que tenemos que hacer para llevar a cabo nuestro proyecto es actualizar las listas de paquetes en la Raspberry que necesitan actualizarse.
Para eso debemos escribir los siguiente comandos la terminal de la Raspberry Pi.

`sudo apt-get update`

Después de finalizado ese proceso, procedereriamos a escribir otro comando en la terminal.

`sudo apt-get upgrade`

Ahora instalaremos RaspAP, un software de enrutador inalámbrico que nos va a permitir configurar la Raspberry para que funcione como un repetidor. Para instalar el software ejecutaremos el siguiente comando en la terminal de la raspberry y esperaremos a que termine el proceso de descarga.

`sudo curl -sl https://install.raspap.com/ | bash`

Luego de instalar RaspAP, tendremos que configurar la localizacion de nuestra ip, la cual se puede cambiar usando el siguiente comando

`sudo raspi-config.`

Después de realizado eso, procedemos a abrir el navegador Chromium y en la zona de navegación escribiremos *"localhost"*  y le damos a buscar. Esto nos llevará a un cuadro donde nos pediran usuario y contraseña, los cuales son *"admin"* y *"secret"* respectivamente.

![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/raw/68074c6187f178a890c43def50e06726e88ec9f3/Imagen1.jpg)

Nos enviará a la página de control del repetidor de wifi, donde tendremos que iniciar el proceso. Para esto nos dirigimos a la segunda pestaña en el menu a la izquierda de la interfaz. 

![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/blob/master/Imagen2.jpg)

En esta, veremos que la página se divide en 4 pestañas. 
-**Basic**: En esta pestaña podremos cambiar el nombre de nuestra red wifi. 
-**Security**: Acá podremos esencialmente cambiar la contraseña de nuestra red. 
-**Advanced** : Aquí tendremos que activar la opcion WiFiAP Client y desactivar todas las demás.

![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/raw/master/Imagen3.jpg)


------------


> Nota:  Cabe destacar que cada vez que cambiemos alguna configuracion necesitaremos guardar los cambios de inmediato o de otro modo estos se perderán.


------------



Como último paso tendremos que activar nuestro repetidor. Para ello, hay que pinchar el botón que dice *"start hotspot"* .
![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/raw/master/Imagen4.jpg)

Y con esto tendríamos listo nuestro repetidor Wifi.


------------

#### Servidor de archivos compartidos
Para crear el servidor, usaremos una variante del procolo SMB (Server Message Block), llamada Samba. 
Lo primero que haremos sería instalar Samba en la Raspberry Pi. Esto lo haremos con el siguiente comando en la terminal.

`sudo apt update && sudo apt install samba samba-common -y`

Cuando termine la ejecución del comando, ya tendremos instalado Samba .
Ahora solo falta configurar el servidor para compartir una carpeta desde la Raspberry Pi. Para eso solo hace falta modificar un archivo con la configuraciones de Samba. Escribiremos el siguiente comando en la terminal.

`sudo nano /etc/samba/smb.conf`

Con este comando accederemos al archivo en la terminal. El siguiente paso es copiar este texto al final del archivo. 
```
[home] 
   comment = Carpeta de inicio de la Raspberry Pi
   path = /home/pi
   browseable = Yes
   writeable = Yes
   only guest = no
   create mask = 0777
   directory mask = 0777
   public = no
```
Con esto compartiremos la carpeta /home/pi en la red SMB. Ahora si quieres tener una carpeta en otro dispositivo de almacenamiento , debemos copiar el mismo texto otra vez debajo del anterior, y hacer unas leves modificaciones, las cuales serán señaladas a continuación.

```
[home]  #  Cambiar el nombre de la carpeta que aparecerá cuando intentemos
  # acceder a ella desde otro ordenador
   comment = Carpeta de inicio de la Raspberry Pi # Cambiar el comentario (opcional)
   path = /home/pi  # Cambiaremos la ruta señalada por la ruta a la carpeta que queremos compartir desde el dispositivo de almacenamiento.
   browseable = Yes 
   writeable = Yes
   only guest = no
   create mask = 0777
   directory mask = 0777
   public = no  
```

 ![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/raw/master/Imagen5.png)
Guardaremos los cambios y volveremos al terminal. 
Ahora procederemos a agregar un permiso de usuario y una contraseña, de manera que solo las personas que tengan esas credenciales podrán acceder a los archivos que tengamos en nuestro servidor. Para eso escribiremos el siguiente comando

------------


> Nota:  En nuestro caso utilizamos de nombre de usuario "pi" , pero tú puedes poner el cual desees.


------------

`sudo smbpasswd -a pi`

Por último escribiremos el siguiente comando en la terminal para reiniciar el servidor 

`
sudo service smbd restart
`

¡Listo!. Ya tendriamos configurado nuestro servidor de archivos compartidos.
Ahora solo hace falta acceder a éste desde el explorador de archivos de nuestro computador. Para eso solo debemos escribir la IP en la barra de direcciones del explorador, con el siguiente formato  ** \\\IP_DE_RASPBERRY_PI.**

![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/raw/master/Imagen6.png)


Nos saldrá una ventana de Windows, pidiendonos un usuario y una contraseña, los cuales son los que hemos registrado anteriormente en la terminal. 

![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/raw/master/Imagen7.png)


Ingresamos lo que nos pide, y listo ya accederiamos al servidor con las carpetas.

![](https://gitlab.com/francisco.espinosa/proyecto-intr-ing-grupo-16/-/raw/master/Imagen7.png)




###End

